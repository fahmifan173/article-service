package services

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

func TestListAllPages(t *testing.T) {

	t.Run("should return All Pages", func(t *testing.T) {
		//GIVEN
		repo := repository.CreateInMemoryPageStorage()
		ps := NewPageService(repo)
		page1, _ := model.CreatePage(ps, "lorem ipsum", "lorem-ipsum", "hello world")
		page2, _ := model.CreatePage(ps, "lorem ipsum", "lorem-ipsum2", "hello world")
		page3, _ := model.CreatePage(ps, "lorem ipsum", "lorem-ipsum3", "hello world")
		repo.SavePage(page1)
		repo.SavePage(page2)
		repo.SavePage(page3)

		// WHEN
		list := ps.GetAllPages()

		// THEN
		assert.NotEmpty(t, list)
		assert.Equal(t, 3, len(list))
	})

}
