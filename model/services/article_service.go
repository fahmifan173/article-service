package services

import (
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

type ArticleServiceInMemory struct {
	Repository *repository.ArticleStorage
}

func NewArticleService(repo *repository.ArticleStorage) model.PostService {
	return &ArticleServiceInMemory{
		Repository: repo,
	}
}

func (a *ArticleServiceInMemory) CheckSlugIsExist(slug string) bool {
	exist := a.Repository.IsSlugExist(slug)
	return exist
}

func (a *ArticleServiceInMemory) GetPublishedArticleBySlug(slug string) (model.Article, error) {
	article, ok := a.Repository.GetPublishedArticleBySlug(slug)
	if ok != nil {
		return model.Article{}, ok
	}
	return article, ok
}

func (a *ArticleServiceInMemory) GetPublishedArticleByCategory(category model.Category) ([]model.Article, error) {
	articles, err := a.Repository.GetPublishedArticleByCategory(category)
	if err != nil {
		return nil, err
	}
	return articles, nil
}
