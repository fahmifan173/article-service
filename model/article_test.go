package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type MockArticleServiceSuccess struct {
}

func (as MockArticleServiceSuccess) CheckSlugIsExist(name string) bool {
	return false
}

func (as MockArticleServiceSuccess) GetPublishedArticleBySlug(slug string) (Article, error) {
	return Article{}, nil
}

func (as MockArticleServiceSuccess) GetPublishedArticleByCategory(category Category) ([]Article, error) {
	return []Article{
		Article{ Name: "1" },
		Article{ Name: "2" },
	}, nil
}

type MockArticleServiceError struct {
}

func (as MockArticleServiceError) CheckSlugIsExist(name string) bool {
	return true
}

func (as MockArticleServiceError) GetPublishedArticleBySlug(slug string) (Article, error) {
	return Article{}, nil
}

func (as MockArticleServiceError) GetPublishedArticleByCategory(category Category) ([]Article, error) {
	return nil, nil
}

func TestCanCreateArticleWithOneCategory(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic}
	as := MockArticleServiceSuccess{}

	// When
	got, err := CreateArticle(as, name, slug, body, category...)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Equal(t, category, got.Category)
	assert.NoError(t, err)

	t.Run("Checking whether article status is notPublished", func(t *testing.T) {
		// Given
		name := "Ikan Asin"
		slug := "ikan-asin"
		body := "Kasus ikan asin menjadi viral"
		var category = []Category{Politic}
		status := ArticleNotPublished

		// When
		got, err := CreateArticle(as, name, slug, body, category...)

		// Then
		assert.NoError(t, err)
		assert.Equal(t, status, got.Status)
	})
}

func TestCantCreateArticleIfCategoryInvalid(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{"budaya"}
	as := MockArticleServiceSuccess{}
	// When
	_, err := CreateArticle(as, name, slug, body, category...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category not valid", err.Error())
}

func TestCanCreateArticleWithCategories(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic, News, Entertainment}
	as := MockArticleServiceSuccess{}
	// When
	got, err := CreateArticle(as, name, slug, body, category...)

	// Then
	assert.Equal(t, name, got.Name)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Equal(t, category, got.Category)
	assert.NoError(t, err)
}

func TestCantCreateArticleWithoutCategory(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	as := MockArticleServiceSuccess{}
	// When
	_, err := CreateArticle(as, name, slug, body)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Category needed", err.Error())
}

func TestCanCreateArticleWithUniqueSlug(t *testing.T) {
	//Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic}
	as := MockArticleServiceSuccess{}

	// When
	_, err := CreateArticle(as, name, slug, body, category...)

	// Then
	assert.Empty(t, err)
}

func TestCantCreateArticleWithExistSlug(t *testing.T) {
	//Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Kasus ikan asin menjadi viral"
	var category = []Category{Politic}
	as := MockArticleServiceError{}

	// When
	_, err := CreateArticle(as, name, slug, body, category...)

	// Then
	assert.NotEmpty(t, err)
	assert.Equal(t, "Change slug, slug is already used", err.Error())
}

func TestCanCreateArticleWithNoSlug(t *testing.T) {
	//Given
	name := "Ikan Asin"
	slug := ""
	body := "Kasus ikan asin menjadi viral"
	as := MockArticleServiceSuccess{}
	var category = []Category{Politic}

	// When
	got, err := CreateArticle(as, name, slug, body, category...)

	// Then

	assert.Empty(t, err)
	assert.NotEmpty(t, got.Slug)
}

func TestIsContainsCategory(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := ""
	body := "Kasus ikan asin menjadi viral"
	as := MockArticleServiceSuccess{}
	category := []Category{Politic, Internet}
	article, _ := CreateArticle(as, name, slug, body, category...)

	// When
	t.Run("category exist", func(t *testing.T) {
		got := article.IsContainsCategory(Internet)
		// Then
		assert.True(t, got)
	})

	// When
	t.Run("category not exist", func(t *testing.T) {
		got := article.IsContainsCategory(News)
		// Then
		assert.False(t, got)
	})
}
